<?php

namespace SchLabs\LaravelSchlabsQr;

use SchLabs\LaravelSchlabsQr\Exceptions\TypeNotFoundException;
use SchLabs\LaravelSchlabsQr\Types\Invitation;
use SchLabs\LaravelSchlabsQr\Types\QrType;

class QrFactory
{

    /**
     * @throws TypeNotFoundException
     */
    public static function getType(string $type): QrType
    {
        switch ($type){
            case Qr::INVITATION:
                return new Invitation();
            default:
                throw new TypeNotFoundException;
        }
    }

}
