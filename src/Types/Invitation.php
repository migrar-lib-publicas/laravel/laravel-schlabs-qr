<?php

namespace SchLabs\LaravelSchlabsQr\Types;

use SchLabs\LaravelSchlabsQr\Qr;
use SchLabs\LaravelSchlabsQr\Types;

class Invitation extends QrType
{
    public function getIdentifier(): string
    {
        return Qr::INVITATION;
    }

    public function getProperties(): array
    {
        return [
            'user_profile_id' => 'int',
            'user_id' => 'int',
            'timestamp' => 'int',
            'community_id' => 'int',
            'profile_id' => 'int',
            'timestamp_qr' => 'int',
            'visitor_profile_id' => 'int',
            'visitor_id' => 'int',
            'schedule' => 'boolean'
        ];
    }

}
