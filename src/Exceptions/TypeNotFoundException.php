<?php

namespace SchLabs\LaravelSchlabsQr\Exceptions;

use Throwable;

class TypeNotFoundException extends \Exception
{
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct('Schlabs QR type not found', $code, $previous);
    }
}
