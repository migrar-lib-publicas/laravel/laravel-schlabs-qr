<?php

namespace SchLabs\LaravelSchlabsQr\Types;

abstract class QrType
{
    /**
     * Associative Array of properties of type and its type
     *
     * ex: [
     *    'property_1' => 'string'
     * ]
     * 
     * property types supported: string, boolean and int
     * 
     * @return array
     */
    abstract public function getProperties(): array;

    abstract public function getIdentifier(): string;

    public function propertyCount(): int
    {
        return count($this->getProperties()) + 1;
    }

}
