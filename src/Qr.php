<?php

namespace SchLabs\LaravelSchlabsQr;

use SchLabs\LaravelSchlabsQr\Exceptions\IncorrectNumberOfSegmentsException;
use SchLabs\LaravelSchlabsQr\Exceptions\TypeNotFoundException;
use SchLabs\LaravelSchlabsQr\Types\QrType;
use SchLabs\LaravelSchlabsQr\QrFactory;

class Qr
{
    /** Type identifier */
    const INVITATION = 'I';


    const SEPARATOR = '|';

    /**
     * @throws TypeNotFoundException
     * @throws IncorrectNumberOfSegmentsException
     */
    public static function hashToArray(string $hash): array
    {
        $hashData = explode(self::SEPARATOR, $hash);
        $qrType = QrFactory::getType($hashData[0]);

        $typeProperties = $qrType->getProperties();
        $propertyTypes = array_values($qrType->getProperties());
        $propertyNames = array_keys($typeProperties);

        self::validateLength($qrType, count($hashData));

        $newData = [];
        $hashData = array_splice($hashData, 1, $qrType->propertyCount());

        foreach ($hashData as $segment => $segmentData){
            $newData[$propertyNames[$segment]] = self::convertType($segmentData, $propertyTypes[$segment]);
        }

        return $newData;
    }

    /**
     * @throws TypeNotFoundException
     * @throws IncorrectNumberOfSegmentsException
     */
    public static function arrayToHash(array $data, string $type): string
    {
        $qrType = QrFactory::getType($type);
        self::validateLength($qrType, count($data) + 1);

        $data = array_map(fn($d) => self::dataToString($d), $data);

        $hash = implode(self::SEPARATOR, $data);

        return $qrType->getIdentifier() . '|' . $hash;
    }

    /**
     * @throws IncorrectNumberOfSegmentsException
     */
    public static function validateLength(QrType $qrType, int $length)
    {
        if($qrType->propertyCount() !== $length){
            throw new IncorrectNumberOfSegmentsException(
                $qrType->getIdentifier(),
                $qrType->propertyCount(),
                $length
            );
        }
    }

    public static function convertType($data, string $type)
    {
        $newData = $data;

        if(settype($newData, $type)){
            return $newData;
        }

        return $data;
    }

    private static function dataToString($data)
    {
        return !is_string($data) ? var_export($data, true): $data;
    }

}
