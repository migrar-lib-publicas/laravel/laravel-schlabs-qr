<?php

namespace SchLabs\LaravelSchlabsQr\Exceptions;

use Throwable;

class IncorrectNumberOfSegmentsException extends \Exception
{
    public function __construct(string $type, int $count, int $has)
    {
        parent::__construct("Incorrect number of segments for type \"${type}\". Must have " . $count .", but has ${has}");
    }
}
