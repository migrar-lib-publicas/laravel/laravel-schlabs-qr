   
# laravel-middleware
  

## Description

Middlewares for Laravel projects

## Depencencies

* schlabs/laravel-api-exception
  

## Installation
  

**In your composer.json**

Add the following code to the "repositories" key:

```json
"repositories": [
	{
		"type": "vcs",
		"url": "https://gitlab.com/migrar-lib-publicas/laravel/laravel-middleware"
	}
]
```

Add this line to your require dependecies:

```json
"schlabs/laravel-middleware": "1.0.*"
```
 
**In your project root run:**
```sh
composer update
```

Finally run
```bash 
php artisan vendor:publish
``` 
and choose 
```
Provider: SchLabs\Middleware\MiddlewareServiceProvider
```

## Middleware  included:
---

### **Belongs to Community middleware:**

Check if the current user is part of the community given in the URL

**Usage:**
Include the middelware in your ``app/Http/Kernel.php`` file.
```
protected $routeMiddleware = [
    ...
    'belongs.community' => \SchLabs\Middleware\Http\Middleware\BelongsToCommunity::class
    ...
];
```
Protect your routes using middleware rule:
```
Route::middleware(['belongs.community'])->group(function(){
    ...
});

```

Options from configuration file config/middleware.php

/** Enables the middleware*/
'enabled' => env('BELONGS_TO_COMMUNITY_ENABLED', true),

/**
    * Roles (Separated by coma (,)) that has access to all routes         
    * 
    * Ex: SysAdmin,Raspberry
    */
'role_passthrough' => explode(',', env('BELONGS_TO_COMMUNITY_ROLE_PASSTHROUGH', 'SysAdmin,Raspberry')),

/** Force the use of the middleware, even for the passthrough roles */
'force' => env('BELONGS_TO_COMMUNITY_FORCE', false)
